from fastapi import FastAPI
from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field
from typing import List
import pandas as pd
from mlflow import MlflowClient, pyfunc
from mlflow import MlflowClient, register_model
from mlflow.entities import ViewType
import mlflow
import sys 
import os 
import shutil
sys.path.append(os.path.abspath('.')) 
sys.path.append(os.path.abspath('..'))
sys.path.append(os.path.abspath('/workspace'))
print(os.path.abspath('/workspace'))

app = FastAPI()
os.environ["MLFLOW_TRACKING_URI"] = "http://172.30.234.99:5000"
client = MlflowClient()
if eval(os.getenv("TEST_MODE")):
    exp_name = 'test_' + os.getenv("EXP_NAME")
    print(exp_name)
    print(exp_name == 'test_IMDB')
else:
    exp_name = os.getenv("EXP_NAME")
print(mlflow.get_experiment_by_name(exp_name))
print(1234567)
run_info = client.search_runs(
    experiment_ids=mlflow.get_experiment_by_name(exp_name).experiment_id,
    filter_string="",
    run_view_type=ViewType.ACTIVE_ONLY,
    max_results=1,
    order_by=["metrics.accuracy DESC"],
)
# which_run = run_info[0].info['artifact_uri']
    
app.loaded_model = pyfunc.load_model('runs:/' + client.get_latest_versions(name = exp_name)[0].run_id + '/final_model')
# app.loaded_model = pyfunc.load_model(client.get_latest_versions(name = "IMDB")[0].source )


class Item(BaseModel):
    USER_MSG: List[str] = Field(None, description="User message")

@app.post(f'/inference')
def create_item(item: Item):
    return JSONResponse(app.loaded_model.predict(pd.DataFrame({'text': item.USER_MSG})))
