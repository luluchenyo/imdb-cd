from transformers import TrainingArguments, Trainer, DataCollatorWithPadding, EarlyStoppingCallback
import numpy as np
import evaluate
from transformers import AutoTokenizer, AutoModelForSequenceClassification
from mlflow import MlflowClient, register_model, pyfunc
from mlflow.entities import ViewType
import mlflow
import subprocess
import os
import pandas as pd
from III_Data_Preparation import data_preparation


class wg_trainer():
    def __init__(self,
                 exp_name="IMDB",
                 data_seed=868,
                 batch_size=2,
                 eval_steps=2,
                 model_name="w11wo/javanese-roberta-small-imdb-classifier",
                 patience=3,
                 output_dir="/mlflow/runs/IMDB",
                 dataset_name="imdb",
                 test_mode=False,
                 **kwargs):
        run_name = f'distilbert-{dataset_name}-seed{data_seed}-bat{batch_size}-eval{eval_steps}'
        if test_mode:
            self.run_name = 'test_' + run_name
            self.exp_name = 'test_' + exp_name
            self.model_path_file = '/mlflow/test_model_path.csv'
        else:
            self.run_name = run_name
            self.exp_name = exp_name
            self.model_path_file = '/mlflow/model_path.csv'
        
        self.data_seed = data_seed
        self.batch_size = batch_size
        self.eval_steps = eval_steps
        self.model_name = model_name
        self.patience = patience
        self.output_dir = os.path.join(output_dir, self.run_name)
        self.dataset_name = dataset_name
        
        os.environ['HF_MLFLOW_LOG_ARTIFACTS'] = '1'  # Log artifacts
        os.environ['MLFLOW_EXPERIMENT_NAME'] = self.exp_name  # Name of the experiment
        os.environ['MLFLOW_TAGS'] = '{"stage": "dev", "user": "lulu"}'  # Tags for the run
        os.environ['MLFLOW_NESTED_RUN'] = '1'  # Enable nested runs
        os.environ['MLFLOW_RUN_ID'] = ''  # Specific run ID to resume
        os.environ['MLFLOW_FLATTEN_PARAMS'] = 'False'  # Do not flatten parameters
        os.environ["MLFLOW_TRACKING_URI"] = "http://172.30.234.99:5000"

        self.client = MlflowClient()
        
    def train(self, ds):
            
        output_dir = f'{self.output_dir}/{self.run_name}'

        id2label = {0: "NEGATIVE", 1: "POSITIVE"}
        label2id = {"NEGATIVE": 0, "POSITIVE": 1}

        model = AutoModelForSequenceClassification.from_pretrained(
            self.model_name, 
            num_labels=2, 
            id2label=id2label, 
            label2id=label2id,
            )
        tokenizer = AutoTokenizer.from_pretrained(self.model_name)

        def compute_metrics(eval_preds):
            metric = evaluate.load("accuracy")
            logits, labels = eval_preds
            predictions = np.argmax(logits, axis=-1)
            return metric.compute(predictions=predictions, references=labels)

        training_args = TrainingArguments(num_train_epochs=1,
                                          output_dir=self.output_dir,
                                          run_name=self.run_name,
                                          per_device_train_batch_size=self.batch_size,
                                          per_device_eval_batch_size=self.batch_size,
                                          evaluation_strategy="steps",
                                          eval_steps=self.eval_steps,
                                          logging_steps=self.eval_steps,
                                          save_steps=self.eval_steps,
                                          save_total_limit=1,
                                          load_best_model_at_end=True,
                                          data_seed=self.data_seed,
                                          )

        data_collator = DataCollatorWithPadding(tokenizer)

        trainer = Trainer(
            model=model,
            tokenizer=tokenizer,
            data_collator=data_collator,
            args=training_args,
            train_dataset=ds["train"],
            eval_dataset=ds["test"],
            compute_metrics=compute_metrics,
            callbacks=[
                EarlyStoppingCallback(early_stopping_patience=self.patience),
                # integrations.MLflowCallback,
            ],
        )

        trainer.train()
        trainer.save_model(os.path.join(self.output_dir, 'final_model'))
        accu = trainer.predict(ds["test"]).metrics['test_accuracy']
        
        
        try:
            df = pd.read_csv(self.model_path_file)
            df = df._append([{'model': os.path.join(self.output_dir, 'final_model'), 'accuracy': accu}]).to_csv(self.model_path_file, index=False)
            print('read')
        except:
            pd.DataFrame([{'model': os.path.join(self.output_dir, 'final_model'), 'accuracy': accu}]).to_csv(self.model_path_file, index=False)
            print('save first')

    def mlflow_register(self, run_name, **kwargs):
        
        run_info = mlflow.search_runs(filter_string=f"run_name=\"{run_name}\"")
        print(run_info)
        print(run_name)
        run_id = run_info.loc[0, 'run_id']

        # output_dir_ = '{}/mlflow/mlartifacts/{}/{}/artifacts'.format(os.path.expanduser('~'), run_info.loc[0, 'experiment_id'], run_info.loc[0, 'run_id'])
        # python_model = partial(python_model, output_dir)

        def python_model(data):
            from transformers import pipeline
            best_ckpt = max([x for x in os.listdir(self.output_dir) if 'checkpoint' in x])
            # best_ckpt = 'final_model'
            model = pipeline(
                "text-classification",
                model=os.path.join(self.output_dir, best_ckpt),
                tokenizer=os.path.join(self.output_dir, best_ckpt),
                device=-1,
                )
            # print(list(data['text'])
            out = model(list(data['text']), truncation=True, return_all_scores=False)
            # out = [o['label'] for o in out]
            return out

        try:
            mlflow.start_run(run_id=run_id)
        except Exception as e:
            print(e)
        mlflow.pyfunc.log_model(
            artifact_path='final_model',
            python_model=python_model,
            # data_path=os.path.join(self.output_dir, 'final_model'),
            code_path=[os.path.join(os.getcwd(), x) for x in os.listdir(os.getcwd())],
            pip_requirements=[
                'mlflow==2.8.0',
                'cloudpickle==2.2.1',
                'transformers',
                'datasets',
                'torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cpu',
                ],
        )
        mlflow.end_run()
        
        
        # register model
        run_info = self.client.search_runs(
            experiment_ids=mlflow.get_experiment_by_name(self.exp_name).experiment_id,
            filter_string="",
            run_view_type=ViewType.ACTIVE_ONLY,
            max_results=1,
            order_by=["metrics.accuracy DESC"],
        )
        # model_uri = 'runs:/' + run_info[0].info.run_id + '/' + max(os.listdir(self.output_dir))
        model_uri = 'runs:/' + run_info[0].info.run_id + '/final_model'
        register_model(model_uri=model_uri, name=self.exp_name)
        
    def registered_model_test_available(self):
        # self.client.get_latest_versions(name = self.exp_name)[0].source

        # Load model as a PyFuncModel.
        loaded_model = pyfunc.load_model('runs:/' + self.client.get_latest_versions(name = self.exp_name)[0].run_id + '/final_model')

        data = pd.DataFrame([{'text': '爛得要死'}])
        print(data)
        print(loaded_model.predict(data))
        return loaded_model.predict(data)
    

if __name__ == '__main__':
    ds = data_preparation()
    args = {'data_seed':878, 
                'batch_size': 1, 
                'eval_steps': 120, 
                'model_name': "w11wo/javanese-roberta-small-imdb-classifier", 
                'patience': 3, 
                'output_dir': "/mlflow/runs/IMDB",
                'test_mode': False,
                'dataset_name': 'imdb',
                'exp_name': 'IMDB',
                }
    trainer = wg_trainer(**args)
    trainer.train(ds)
    # trainer.mlflow_register()
    trainer.registered_model_test_available()
