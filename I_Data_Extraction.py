from datasets import load_dataset
import pandas as pd
import os
def data_extraction(test_mode=False):
    split = ['train[:1%]', 'test[:1%]', 'test[1%:2%]']
    ds = load_dataset("imdb", split=split)
    if test_mode:
        data_dir = 'test_data'
    else:
        data_dir = 'data'
    os.makedirs(f'/mlflow/{data_dir}/raw_data', exist_ok=True)
    
    pd.DataFrame(ds[0]).to_csv(f'/mlflow/{data_dir}/raw_data/raw_train.csv', index=False)
    pd.DataFrame(ds[1]).to_csv(f'/mlflow/{data_dir}/raw_data/raw_val.csv', index=False)
    pd.DataFrame(ds[2]).to_csv(f'/mlflow/{data_dir}/raw_data/raw_test.csv', index=False)
    print(len(ds[0]))
    assert len(ds[0]) > 10

if __name__ == '__main__':
    data_extraction()