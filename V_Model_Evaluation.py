from transformers import Trainer, DataCollatorWithPadding
import numpy as np
import evaluate
from transformers import AutoTokenizer, AutoModelForSequenceClassification
import pandas as pd
import os
from III_Data_Preparation import data_preparation

def get_accuracy(model_path, ds):
    id2label = {0: "NEGATIVE", 1: "POSITIVE"}
    label2id = {"NEGATIVE": 0, "POSITIVE": 1}


    model = AutoModelForSequenceClassification.from_pretrained(
        model_path, 
        num_labels=2, 
        id2label=id2label, 
        label2id=label2id,
        )
    tokenizer = AutoTokenizer.from_pretrained(model_path)

    def compute_metrics(eval_preds):
        metric = evaluate.load("accuracy")
        logits, labels = eval_preds
        predictions = np.argmax(logits, axis=-1)
        return metric.compute(predictions=predictions, references=labels)

    data_collator = DataCollatorWithPadding(tokenizer)

    trainer = Trainer(
        model=model,
        tokenizer=tokenizer,
        data_collator=data_collator,
        compute_metrics=compute_metrics,
    )
    accu = trainer.predict(ds).metrics['test_accuracy']
    return accu
def model_evaluation():
    model_path = pd.read_csv('/mlflow/model_path.csv')['model'].iloc[-1]
    ds = data_preparation()['test']
    accu = get_accuracy(model_path=model_path, ds=ds)
    print('current model accuracy: ', accu)
    assert accu > 0.8

if __name__ == '__main__':
    model_evaluation()