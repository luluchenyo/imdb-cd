# Use an official Python runtime as a parent image
FROM python:3.10.12-slim-bookworm


# RUN pip install --upgrade pip
# RUN pip install -r /root/${REQUIREMENT_FILE}
RUN pip install gunicorn fastapi uvicorn[standard] pandas==2.1.2 mlflow==2.8.0 cloudpickle==2.2.1 transformers==4.35.0 datasets==2.14.6 evaluate
# RUN pip install lightning
# RUN pip install lightning
# RUN pip install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cu118

RUN pip install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cpu
# RUN rm -rf /root/.cache/pip
# RUN rm -rf /root/${REQUIREMENT_FILE}

# RUN useradd --create-home 1003
# RUN chown -R 1003:1003 /home/1003 \
#     && chmod 777 /home/1003
# USER 1003
# WORKDIR /home/1003
# RUN mkdir -p /home/1003/model

# RUN pip freeze > /home/1003/requirements.txt
# USER root

# 執行 chown 命令

EXPOSE ${INFERENCE_PORT}

CMD ["bash"]