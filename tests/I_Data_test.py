import datasets
import pandas as pd
from datasets import Dataset, DatasetDict

import sys 
import os 
sys.path.append(os.path.abspath('.')) 
from I_Data_Extraction import data_extraction
from II_Data_Validation import data_validation
from III_Data_Preparation import data_preparation
class TestData:
    def setup_method(self):
        data_extraction(test_mode=True)
        data_validation(test_mode=True)
        self.test_ds = data_preparation(test_mode=True)
    def test_data_extraction(self):
        assert len(pd.read_csv('/mlflow/data/raw_data/raw_train.csv')) > 0
    def test_tokenizer(self):
        assert 'input_ids' in self.test_ds['train'].features
        
        

    