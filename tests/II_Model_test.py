from transformers import AutoTokenizer
import pandas as pd
from datasets import Dataset, DatasetDict

import sys 
import os 
sys.path.append(os.path.abspath('.')) 
sys.path.append(os.path.abspath('..')) 
sys.path.append('/workspace')

from IV_Model_Training import wg_trainer
class TestModel:
    def setup_method(self):
        self.model = "w11wo/javanese-roberta-small-imdb-classifier"
        args = {'data_seed':888, 
                'batch_size': 1, 
                'eval_steps': 2, 
                'model_name': self.model, 
                'patience': 3, 
                'output_dir': "/mlflow/runs/IMDB",
                'test_mode': True,
                'dataset_name': 'imdb',
                'exp_name': 'IMDB',
                }
        self.wg_trainer = wg_trainer(**args)
        def get_test_data():
            dft = pd.DataFrame({'text': ['爛得要死!!!#fsdfs', '太讚了吧', 'so crazy', 'pokemon go', 'wowowowowowowowo'], 'label': [0, 1, 0, 1, 1]})
            dst = Dataset.from_pandas(dft)
            dfv = pd.DataFrame({'text': ['爛得可以', '不能再更棒了', 'so amazing', 'freak', 'hahahahahaha'], 'label': [0, 1, 1, 0, 1]})
            dsv = Dataset.from_pandas(dfv)
            dftt = pd.DataFrame({'text': ['爛得可以fsdfsdf', '不能再更fs棒了', 'so amazinasdg', 'freagdgk', 'hahaasdhahahaha'], 'label': [0, 1, 1, 0, 1]})
            dstt = Dataset.from_pandas(dftt)
            ds = DatasetDict()
            ds['train'] = dst
            ds['validation'] = dsv
            ds['test'] = dstt
            def tokenizer(dataset, model_name="w11wo/javanese-roberta-small-imdb-classifier"):
                def tokenize(examples):
                    tokenizer = AutoTokenizer.from_pretrained(model_name)
                    outputs = tokenizer(examples['text'], truncation=True)
                    return outputs
                return dataset.map(tokenize, batched=True)
            return tokenizer(ds)
        self.test_data = get_test_data()

    def test_train(self):
        self.wg_trainer.train(self.test_data)
    def test_mlflow_register(self):
        self.wg_trainer.mlflow_register(self.wg_trainer.run_name)
    def test_registered_model_test_available(self):
        print(self.wg_trainer.registered_model_test_available())
        assert self.wg_trainer.registered_model_test_available() is not None