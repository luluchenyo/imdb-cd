import pandas as pd
import re
import os
def data_validation(test_mode=False):
    if test_mode:
        data_dir = 'test_data'
    else:
        data_dir = 'data'
        
    df = pd.concat([pd.read_csv(f'/mlflow/{data_dir}/raw_data/{x}') for x in os.listdir(f'/mlflow/{data_dir}/raw_data')], axis=0)
    df['length'] = df['text'].apply(lambda x: len(x))
    pattern = r"[^\w\s]"  # This pattern matches anything that is not a word character or whitespace
    df['symbols_ratio'] = df['text'].apply(lambda x: len(re.findall(pattern, x)))
    df['symbols_ratio'] = df['symbols_ratio'] / df['length']
    df[['length', 'symbols_ratio']].describe().to_csv(f'/mlflow/{data_dir}/data_description.csv')
    assert df['symbols_ratio'].iloc[-1] < 0.2
    
if __name__ == '__main__':
    data_validation()