from datasets import load_dataset
from transformers import AutoTokenizer
import os
def data_preparation(test_mode=False, model_name="w11wo/javanese-roberta-small-imdb-classifier"):
    if test_mode:
        data_dir = 'test_data'
    else:
        data_dir = 'data'
    ds = load_dataset(f'/mlflow/{data_dir}/raw_data')
    def tokenize(examples):
        tokenizer = AutoTokenizer.from_pretrained(model_name)
        outputs = tokenizer(examples['text'], truncation=True)
        return outputs
    ds = ds.map(tokenize, batched=True)
    return ds

if __name__ == '__main__':
    data_preparation()