import requests

def alive_test_inference_api():
    url = 'http://172.30.234.99:16888/inference'
    headers = {"Content-Type": "application/json"}
    data = {"USER_MSG": ["amazing"]}
    response = requests.post(url, json=data, headers=headers)
    print(response.json())
    return response


if __name__ == '__main__':
    out = alive_test_inference_api().json()
    assert 'label' in out[0]
    